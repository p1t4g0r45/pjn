package pjn.lab6

import morfologik.stemming.polish.PolishStemmer
import pjn.lab1.Metrics

import scala.collection.immutable.{Map => IMap, Seq => ISeq}
import scala.collection.mutable
import scala.io.Source

/**
  * Created by baran on 04.04.17.
  */
case class Note(id: String, text: List[String], originalText: String)

case class NoteVector(id: String, vector: ISeq[Double])

case class NoteGraph(id: String, map: IMap[(String, String), Double])

object Cache {

  val filename = "pjn_lab6/pap.txt"
  val pattern = """(\d{6})(.*)""".r
  lazy val stemmer = new PolishStemmer()
  lazy val notes = getNotes(readFile())
  lazy val notesLength = notes.length

  lazy val df = notes.flatMap(_.text.distinct).groupBy(identity).mapValues(_.length)
  lazy val terms = df.keys.zipWithIndex.toMap
  lazy val termsLength = terms.size

  def readFile() = Source.fromFile(filename).getLines()

  def getNotes(lines: Iterator[String]) = lines.mkString(" ").split("#").tail.map { line =>
    val pattern(id, text) = line
    Note(id, text.toLowerCase.replaceAll("[^\\p{L} ]+", "").split(" ").map(stem).toList, text)
  }.toList

  def stem(word: String) = {
    val lookup = stemmer.lookup(word)
    if (lookup.size() == 0) {
      word
    } else {
      lookup.get(0).getStem.toString
    }
  }
}

object `tf-idf` {

  def `tf-idf`(term: String, document: Note) = tf(term, document) * idf(term)

  def tf(term: String, document: Note) = document.text.count(_ == term)

  def idf(term: String) = math.log(Cache.notesLength / Cache.df(term))

  def note2vector(note: Note) = NoteVector(note.id, {
    val array = Array.fill(Cache.termsLength)(0d)
    var sum = 0d
    note.text.foreach { term =>
      val d1 = `tf-idf`(term, note)
      sum += d1
      array(Cache.terms(term)) = d1
    }
    //normalize
    note.text.foreach { term =>
      array(Cache.terms(term)) /= sum // sum above `tf-idf` values
    }
    array.toList
  })
}

object Graph {

  val k = 3

  def note2vector(note: Note) = NoteGraph(note.id, {
    val map = mutable.Map.empty[(String, String), Double]
    note.text.sliding(k).foreach { ngram =>
      ngram.foreach { v =>
        map.update((v, ngram.head), map.getOrElse((v, ngram.head), 1d))
      }
    }
    map.toMap
  })
}

//lab 7 - k kilkaset, np 100, odleglosc euklidesowa

object TextSimilarityApp {

  import `tf-idf`.note2vector
  //import Graph.note2vector

  def d(n1: NoteVector, n2: NoteVector) = Metrics.cosinus(n1.vector.toList, n2.vector.toList)

  def d(n1: NoteGraph, n2: NoteGraph) = Metrics.graphCosinus(n1.map, n2.map)

  def main(args: Array[String]): Unit = {
    //println(note2vector(Cache.notes.head))
    val headNote = note2vector(Cache.notes(15581))
    val map: List[(String, Double)] = Cache.notes.tail.map(note => {
      if (note.id.toInt % 100 == 0) println(note.id)
      s"${note.id}: ${note.originalText}" -> d(headNote, note2vector(note))
    }).sortBy(_._2)
    map.take(100).foreach(println)
  }

}