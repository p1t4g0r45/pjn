package pjn.lab2

/**
  * Created by baran on 09.03.17.
  */

import scala.io.Source
import scala.math._

object LevenshteinApp {
  import util.SortHelper._

  val filename = "pjn_lab2/formy.txt"

  val diacreticMistake = 0.5
  val spellingMistake = 0.5
  val czechMistake = 0.5
  val maxDistance = 4

  val diacretic = Seq(
    ('a', 'ą'),
    ('c', 'ć'),
    ('e', 'ę'),
    ('l', 'ł'),
    ('n', 'ń'),
    ('o', 'ó'),
    ('s', 'ś'),
    ('z', 'ż'),
    ('z', 'ź')
  )

  val spelling = Seq(
    ("u", "ó"),
    ("ż", "rz"),
    ("ż", "sz"),
    ("rz", "sz"),
    ("h", "ch"),
    ("n", "ń"),
    ("f", "w"),
    ("n", "ń"),
    ("e", "ę"),
    ("e", "em"),
    ("s", "z"),
    ("trz", "cz"),
    ("ą", "ę"),
    ("ą", "om"),
    ("ą", "on"),
    ("ę", "en"),
    ("ę", "em"),
    ("ą", "oń"),
    ("i", "ji"),
    ("p", "bł")
  )

  def minimum(i1: Double, i2: Double, i3: Double) = min(min(i1, i2), i3)

  def isDiacretic(c1: Char, c2: Char) = diacretic.exists {
    case (s1, s2) if s1 == c1 && s2 == c2 => true
    case _ => false
  }

  def spellingBonus(s1: String, i: Int, s2: String, j: Int) = spelling.map {
    case (c1, c2) if s1.substring(i).startsWith(c1) && s2.substring(j).startsWith(c2) => abs(c1.length - c2.length)
    case (c2, c1) if s1.substring(i).startsWith(c1) && s2.substring(j).startsWith(c2) => abs(c1.length - c2.length)
    case _ => -spellingMistake
  }.max + spellingMistake

  def czechBonus(s1: String, i: Int, s2: String, j: Int) = {
    if (s1.length > i + 1 && s2.length > j + 1 && s1(i) == s2(j + 1) && s2(j) == s1(i + 1)) 1 + czechMistake else 0d
  }

  def distance(s1: String, s2: String, optimize: Boolean = true): Double = {
    val dist = Array.tabulate(s2.length + 1, s1.length + 1) { (j, i) => if (j == 0) i else if (i == 0) j else 0d }

    for (j <- 1 to s2.length; i <- 1 to s1.length) {
      dist(j)(i) = if (s2(j - 1) == s1(i - 1))
        dist(j - 1)(i - 1)
      else if (isDiacretic(s1(i - 1), s2(j - 1)))
        dist(j - 1)(i - 1) + diacreticMistake
      else
        minimum(dist(j - 1)(i), dist(j)(i - 1), dist(j - 1)(i - 1)) + 1 - spellingBonus(s1, i - 1, s2, j - 1) - czechBonus(s1, i - 1, s2, j - 1)
      if (optimize && i == j && dist(j)(i) >= maxDistance) return maxDistance + 1
    }

    dist(s2.length)(s1.length)
  }

  def readFile = Source.fromFile(filename)(io.Codec.ISO8859).getLines.toList

  def predict(ln: String, forms: List[String]) = bottom(10, forms.map(form => form -> distance(ln, form)).filter(_._2 < maxDistance))

  def main(args: Array[String]): Unit = {
    val forms = readFile
    for (ln <- io.Source.stdin.getLines) println(predict(ln, forms))
  }
}