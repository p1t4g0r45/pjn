package pjn.lab1

import scala.collection.mutable.{Map => MMap}
import scala.io.Source

/**
  * Created by baran on 28.02.17.
  */
sealed trait Language

case object Polish extends Language

case object English extends Language

case object German extends Language

case object Finnish extends Language

case object Spanish extends Language

case object Italian extends Language

object LanguageRecognitionApp {

  val n: Int = 2
  val languages: MMap[Language, Map[String, Double]] = MMap.empty

  def main(args: Array[String]): Unit = {
    initLanguages()
    println("Languages initialized. Provide input:")
    for (ln <- io.Source.stdin.getLines) predict(ln)
  }

  def initLanguages(): Unit = {
    languages += English -> parse(Seq(
      "pjn_lab1/Harry Potter 1 Sorcerer's_Stone.txt",
      "pjn_lab1/Harry Potter 2 Chamber_of_Secrets.txt",
      "pjn_lab1/Harry Potter 3 Prisoner of Azkaban.txt",
      "pjn_lab1/Harry Potter 4 and the Goblet of Fire.txt"
    ))
    languages += Polish -> parse(Seq(
      "pjn_lab1/polski.txt",
      "pjn_lab1/polski2.txt",
      "pjn_lab1/polski3.txt"
    ))
    languages += Spanish -> parse(Seq(
      "pjn_lab1/spanish.txt",
      "pjn_lab1/spanish1.txt"
    ))
    languages += Italian -> parse(Seq(
      "pjn_lab1/q.txt"
    ))
    languages += Finnish -> parse(Seq(
      "pjn_lab1/finnish.txt",
      "pjn_lab1/finnish1.txt"
    ))
    languages += German -> parse(Seq(
      "pjn_lab1/2momm10.txt",
      "pjn_lab1/4momm10.txt",
      "pjn_lab1/5momm10.txt",
      "pjn_lab1/8momm10.txt"
    ))
  }

  def parse(fileNames: Seq[String]): Map[String, Double] = parseFiles(fileNames).toMap

  def parseFiles(fileNames: Seq[String]): Seq[(String, Double)] = normalize(
    fileNames.flatMap { filename =>
      val lines = Source.fromFile(filename)(io.Codec.ISO8859).getLines()
      lines.flatMap { line =>
        nGram(n, line)
      }
    }
  )

  def nGram(n: Int, text: String): Array[String] =
    text.toLowerCase.split(' ').flatMap(_.sliding(n).filter(_.matches("[a-zA-Z]*")).map(_.mkString).toList)

  def normalize(seq: Seq[String]): List[(String, Double)] = seq.groupBy(identity).mapValues(_.length.toDouble).toList.sortWith(_._2 > _._2) match {
    case head :: Nil => head._1 -> 1d :: Nil
    case head :: tail => head._1 -> 1d :: tail.map(t => t._1 -> t._2 / head._2)
  }


  def predict(input: String): Unit = {
    import Metrics._
    val (inputNgrams, inputVector) = normalize(nGram(n, input)).unzip
    //println(s"Input vector: $inputVector")
    //println(s"Input n-grams: $inputNgrams")
    languages.map { case (language, nGrams) =>
      val languageVector = inputNgrams.map(nGram => nGrams.getOrElse(nGram, 0d))
      //println(s"$language vector: $languageVector")
      language -> Seq(
        euklid(inputVector, languageVector),
        taxi(inputVector, languageVector),
        max(inputVector, languageVector),
        cosinus(inputVector, languageVector)
      )
    }.toList.sortBy(_._2.head).foreach(println)
  }

}

object Metrics {
  def euklid(v1: List[Double], v2: List[Double]): Double =
    Math.sqrt((v1 zip v2).map { case (x, y) => (x - y) * (x - y) }.sum)

  def taxi(v1: List[Double], v2: List[Double]): Double =
    (v1 zip v2).map { case (x, y) => Math.abs(x - y) }.sum

  def max(v1: List[Double], v2: List[Double]): Double =
    (v1 zip v2).map { case (x, y) => Math.abs(x - y) }.sorted(Ordering[Double].reverse).head

  def cosinus(v1: List[Double], v2: List[Double]): Double =
    1 - ((v1 zip v2).map { case (x, y) => (x * y) -> (if (x != 0 || y != 0) 1 else 0) }.foldLeft((0d, 0))((a, b) => (a._1 + b._1, a._2 + b._2)) match {
      case (sum, length) => sum / length /// (v1.size * v2.size))
    })

  def graphCosinus(v1: Map[(String, String), Double], v2: Map[(String, String), Double]): Double =
    1 - {
      val sum = v1.map { case (word, weight) => weight * v2.find(_._1 == word).map(_._2).getOrElse(0d) }.sum
      sum / (v1.keys ++ v2.keys).toList.distinct.length
    }
}
