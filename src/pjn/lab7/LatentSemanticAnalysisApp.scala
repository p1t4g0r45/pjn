package pjn.lab7

import java.io.PrintWriter

import pjn.lab6.Cache

/**
  * Created by baran on 13.05.17.
  */
object LatentSemanticAnalysisApp {
  val notes = Cache.notes

  def main(args: Array[String]): Unit = {
    val notesAsText = notes.map(note => note.id + " " + note.text.mkString(" "))
    val output = notesAsText.mkString("\n")
    new PrintWriter("pjn_lab7/pap_id") {
      write(notesAsText.length.toString + "\n" + output); close()
    }
  }
}
