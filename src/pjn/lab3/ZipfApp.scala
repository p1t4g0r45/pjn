package pjn.lab3

import java.util.function.Consumer

import morfologik.stemming.WordData
import morfologik.stemming.polish.PolishStemmer
import org.viz.lightning._

import scala.collection.mutable.{ListBuffer, Seq => MSeq}
import scala.io.Source


/**
  * Created by baran on 14.03.17.
  */
case class WeightedWord(word: String, weight: Double)

object WeightedWord {
  def apply(tuple: (String, Double)): WeightedWord = WeightedWord(tuple._1, tuple._2)
}

object ZipfApp {
  val stemmer = new PolishStemmer()
  val filename = "pjn_lab3/potop.txt"

  def stem(word: String) = {
    var buffer = ListBuffer[WeightedWord]()
    val lookup = stemmer.lookup(word)
    if (lookup.size() == 0) {
      List(WeightedWord(word, 1))
    } else {
      lookup.stream().forEach(new Consumer[WordData] {
        override def accept(wd: WordData): Unit = buffer += WeightedWord(wd.getStem.toString, 1d / lookup.size())
      })
      buffer.toList
    }
  }

  def getWords(fileNames: String*): List[String] = fileNames.toList.flatMap(getWords)

  def getWords(fileName: String) = Source.fromFile(fileName).getLines.flatMap(_.split(" ")
    .map(word => word.toLowerCase.replaceAll("[^\\p{L}]+", "")))
    .filter(word => word.matches("[\\p{L}]+"))
    .toList

  //1.
  def countOccurances(words: List[String]) =
    words.flatMap(stem).groupBy { case WeightedWord(word, _) => word }
      .mapValues(_.map(_.weight).sum).toList.map(WeightedWord.apply).sortWith(_.weight > _.weight)

  //3.
  def words50(weightedWords: List[WeightedWord], size: Int) = {
    var sum = 0d
    weightedWords.takeWhile { word =>
      sum += word.weight
      sum < size / 2
    }
  }

  def hapaxLegomena(weightedWords: List[WeightedWord]) = weightedWords.filter(_.weight == 1)

  //4.
  //def mapToBasicForm(words: List[String]) = words.flatMap(stem).map(_.word)
  def mapToBasicForm(words: List[String]) = words.map(stem).map(_.head).map(_.word)

  def nGrams(n: Int, text: List[String]) = text.sliding(n).map(_.mkString(" ")).toList

  def nGramStatistics(words: List[String], n: Int) =
    nGrams(n, mapToBasicForm(words)).groupBy(identity).mapValues(_.length).toList.sortWith(_._2 > _._2)

  def main(args: Array[String]): Unit = {
    val words = getWords(filename)
    //1
    val weightedWords = countOccurances(words)
    weightedWords.reverse.foreach(println)

    //2
    /*val lgn = Lightning(host = "http://localhost:3000")
    lgn.useSession("3b2f0941-e8ef-4687-b7c6-4a99a3eb46b5")

    val d = 35.176202
    val B = 1.5482
    val P = 3445520
    lgn.line(
      Array(weightedWords.map(_.weight).map(math.log).toArray,
      Array.tabulate(weightedWords.size)(elem => weightedWords.head.weight / (elem + 1)).map(math.log),
      Array.tabulate(weightedWords.size)(elem => P / math.pow(elem + d + 1, B)).map(math.log))
    )
    lgn.line(Array(weightedWords.map(_.weight).take(100).toArray))*/


    //3
    println(s"All words: ${words.size}")
    println(s"All unique words: ${weightedWords.size}")
    println(s"50% words: ${words50(weightedWords, words.size).size}")
    println(s"hapax legomena: ${hapaxLegomena(weightedWords).size}")

    //4
    val statistics = nGramStatistics(words, 3)
    statistics.reverse.foreach(println)
  }
}
