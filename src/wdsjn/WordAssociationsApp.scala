package wdsjn

import java.io.{File, PrintWriter}

import morfologik.stemming.polish.PolishStemmer
import util.SortHelper

import scala.collection.mutable.{Map => MMap}
import scala.io.{Codec, Source}

/**
  * Created by baran on 23.10.17.
  */
object Constants {
  val Filename = "wdsjn/wierzba.txt"
  val Alpha = 0.66
  val Beta = 0.00002
  val Gamma = 0.00002
  val MaxDistance = 12
  val Stimuli = Seq("igła", "krawiec", "nić", "nitka")
  val Files = Seq("igla.csv", "krawiec.csv", "nic.csv", "nitka.csv")
}

object WordAssociationsApp extends App {

  import Constants._
  import Corpus._

  val coOccurrence = MMap[(String, String), Int]()

  readCorpus().sliding(MaxDistance * 2 + 1) // left + word + right
    .filter(window => Stimuli contains window(MaxDistance)) // if middle word is a stimulus
    .foreach(updateCoOccurrence)

  Stimuli.filter(wordOccurrence contains).map(stimulus => stimulus -> associationFor(stimulus)).foreach {
    case (stimulus, list) =>
      println(s"$stimulus:")
      println(list.map { case (word, frequency) => s"\t$word: ${"%.2f".format(frequency)}" }.mkString(", "))
  }

  def updateCoOccurrence(window: Seq[String]) = {
    val stimulus = window(MaxDistance)
    window.filter(_ != stimulus).foreach { word =>
      coOccurrence(stimulus -> word) = coOccurrence.getOrElse(stimulus -> word, 0) + 1
    }
  }

  def associationFor(stimulus: String) = {
    def association(i: String, j: String) =
      (math.pow(corpusSize, Alpha) / wordOccurrence(i)) *
        (coOccurrence.getOrElse(i -> j, 0) / weakenJ(j))

    def weakenJ(j: String) = {
      if (wordOccurrence(j) > Beta * corpusSize)
        math.pow(wordOccurrence(j), Alpha)
      else
        Gamma * corpusSize
    }

    SortHelper.top(10, wordOccurrence.keys.map(j => j -> association(stimulus, j)).toList).reverse
  }

}

object Corpus {

  lazy val (wordOccurrence, corpusSize) = countOccurrences()
  private lazy val Stemmer = new PolishStemmer()

  def readCorpus(): Iterator[String] = fileIterator(Constants.Filename).flatMap(_.split(" ")).map(_.toLowerCase).map(stem)

  private def fileIterator(filename: String) = Source.fromFile(filename)(Codec.UTF8).getLines()

  private def countOccurrences() = {
    var corpusSize = 0
    val map = MMap[String, Int]()
    readCorpus().foreach { word =>
      corpusSize += 1
      map(word) = map.getOrElse(word, 0) + 1
    }
    (map.filter(_._2 > 1), corpusSize)
  }

  def stem(word: String) = {
    val lookup = Stemmer.lookup(word)
    if (lookup.size() == 0) {
      word
    } else {
      lookup.get(0).getStem.toString
    }
  }
}

object ClearListApp extends App {
  val inputFiles = Constants.Files.map("wdsjn/" + _)
  val outputFiles = Constants.Files.map("wdsjn/out_" + _)

  private def readFile(filename: String) = Source.fromFile(filename)(Codec.UTF8).getLines().toSeq

  private def filterMeaningless(lines: Seq[String]) = lines.map(_.split(",")).collect {
    case Array(amount, word) if amount.toInt >= 10 && word != "PUSTE" && !word.contains(" ") => Corpus.stem(word) -> amount
  }

  private def groupWords(words: Seq[(String, String)]) = words.toList
    .groupBy{ case (word, amount) => word}
    .mapValues(list => list.map(_._2.toInt).sum).toList
    .sortBy(_._2).reverse

  private def writeToFile(filename: String, s: String): Unit = {
    val pw = new PrintWriter(new File(filename))
    try pw.write(s) finally pw.close()
  }

  inputFiles.zip(outputFiles).foreach { case (input, output) =>
    val content = groupWords(filterMeaningless(readFile(input))).map { case (word, amount) => s"$word($amount)" }.mkString("\n")
    writeToFile(output, content)
  }
}