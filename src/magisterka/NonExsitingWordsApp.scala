package magisterka

import java.io.File

import morfologik.stemming.polish.PolishStemmer

import scala.io.{Codec, Source}

/**
  * Created by baran on 03.03.18.
  */
object NonExsitingWordsApp extends App {

  private val Stemmer = new edu.stanford.nlp.process.Stemmer()
  private val Dictionary = fileIterator("magisterka/words_alpha.txt").toSeq

  private def fileIterator(filename: String): Iterator[String] = Source.fromFile(filename)(Codec.UTF8).getLines()

  private def exists(word: String) = {
    val stemmed = Stemmer.stem(word.toLowerCase)
    (isInDictionary(word) || isInDictionary(stemmed)) -> word
  }

  private def isInDictionary(word: String) = {
    Dictionary.contains(word)
  }

  private def count(filename: String) = {
    val words = fileIterator(filename).flatMap { line =>
      line.trim().split("[\\n\\s]+").map(_.replaceAll("[^a-zA-Z]", "")).filter(_.nonEmpty)
    }.map(exists)
    .partition(_._1)
    val (existing, nonExisting) = words._1 -> words._2
    val (existingSize, nonExistingSize) = existing.size -> nonExisting.map{ a => printNonExisting(a._2); a}.size
    if (existingSize + nonExistingSize == 0) {
      //println(filename)
      //println("houston")
      0.0
    } else {
      val result = existingSize.toDouble / (existingSize + nonExistingSize)
      //println(result)
      result
    }
  }

  private def printNonExisting(word: String): Unit = {
    // println(word)
  }

  def recursiveListFiles(f: File): Array[File] = {
    val these = f.listFiles
    these.filter(!_.isDirectory) ++ these.filter(_.isDirectory).flatMap(recursiveListFiles)
  }

  val files = recursiveListFiles(new File("/home/baran/PycharmProjects/magisterka/final/5.model_only"))
  val files2 = recursiveListFiles(new File("/home/baran/PycharmProjects/magisterka/final/5.generated_from_80_5"))
  val files3 = recursiveListFiles(new File("/home/baran/PycharmProjects/magisterka/final/5.generated_from_80_20"))
  val files4 = recursiveListFiles(new File("/home/baran/PycharmProjects/magisterka/final/2.without_chars"))

  //println(s"size: ${files.length}")
  val percentage = files.map(_.getAbsolutePath).map(count).sum / files.length
  println(s"model_only: $percentage")
  val percentage2 = files2.map(_.getAbsolutePath).map(count).sum / files2.length
  println(s"generated_from_80_5: $percentage2")
  val percentage3 = files3.map(_.getAbsolutePath).map(count).sum / files3.length
  println(s"generated_from_80_20: $percentage3")
  val percentage4 = files4.map(_.getAbsolutePath).map(count).sum / files4.length
  println(s"without_chars: $percentage4")

  //println(count("/home/baran/PycharmProjects/magisterka/final/5.generated_from_all/comp.graphics/0"))

}
