package pjn.lab4

import pjn.lab2.LevenshteinApp
import pjn.lab3.{WeightedWord, ZipfApp}

/**
  * Created by baran on 22.03.17.
  */
object Laplace {
  val fileNames = Seq("dramat", "popul", "proza", "publ", "wp").map(f => s"pjn_lab4/$f.txt")
  val words: List[String] = ZipfApp.getWords(fileNames: _*)
  val weightedWords = ZipfApp.countOccurances(words)

  val M = weightedWords.size
  val N = weightedWords.map(_.weight).sum

  def `P(c)`(c: String): Double = weightedWords.find(_.word == c).map(_.weight).map(Nc => (Nc + 1) / (N + M)).getOrElse(0)
}

object BayesProbabilityApp {
  import util.SortHelper.top
  val forms = LevenshteinApp.readFile

  /**
    * List((drogi,1.1259110954138959E-6), (oraz,1.5320798046705203E-6),
    * (raz,1.5540809506905934E-6), (rada,1.6990610091758988E-6),
    * (uwaga,1.882559598166896E-6), (drugi,2.226902629359875E-6),
    * (drzazga,2.8411443297888934E-6), (trzeba,3.2146234293608007E-6),
    * (druga,1.2576239829301622E-5), (droga,2.009406039335889E-5))
    */
  def `P(w|c)`(w: String, c: String, optimize: Boolean = true): Double = {
    val d = LevenshteinApp.distance(w, c, optimize) + 1
    1 / math.pow(d, d)
  }

  /**
    * bardzo podobne wyniki do poprzedniego
    * List((jaka,1.38572236550733E-7), (rada,1.4591269660283925E-7),
    * (uwaga,1.616712678359459E-7), (oraz,1.6447025386052063E-7),
    * (raz,1.6683209823710777E-7), (drugi,1.9124290768078772E-7),
    * (drzazga,2.59864371072038E-7), (trzeba,2.760668219725719E-7),
    * (druga,8.416813099813079E-7), (droga,1.3448212903287976E-6))
    */
  def `P(w|c)1`(w: String, c: String, optimize: Boolean = true): Double = {
    val d = LevenshteinApp.distance(w, c, optimize) + 1
    1 / math.exp(2*d)
  }

  /**
    * preferuje poprawki o mniejszej odleglosci levenshteina
    * List((drugi,6.415484837831452E-11), (szata,6.660265070834367E-11),
    * (trzeba,9.261010157561386E-11), (szafa,2.4143460881774583E-10),
    * (brzana,3.220696877164242E-10), (draka,6.441393754328484E-10),
    * (wrzawa,1.073565625721414E-9), (druga,4.1904844923992654E-8),
    * (droga,6.695470952415885E-8), (drzazga,5.501328778765467E-7))
    */
  def `P(w|c)2`(w: String, c: String, optimize: Boolean = true): Double = {
    val d = LevenshteinApp.distance(w, c, optimize) + 1
    1 / math.exp(d*d)
  }

  def `P(c|w)`(c: String, w: String, f: (String, String, Boolean) => Double = `P(w|c)`, optimize: Boolean = true) = f(w, c, optimize) * Laplace.`P(c)`(c)

  def predict(w: String, f: (String, String, Boolean) => Double) = top(10, forms.map(form => form -> `P(c|w)`(form, w, f)))

  def main(args: Array[String]): Unit = {
    //2
    Laplace.weightedWords.reverse.foreach(println)
    println()

    //1 P(w|c)
    val examples = Seq(
      ("żeka", "rzeka"),
      ("żeka", "beka"),
      ("żeka", "parapet"),
      ("drzaga", "droga"),
      ("drzaga", "drzazga")
    )
    examples.foreach { case (w: String, c: String) => println(s"P($w|$c) = ${`P(w|c)`(w, c, optimize = false)}") }
    println()

    //3 P(c|w)
    examples.foreach { case (w: String, c: String) => println(s"P($c|$w) = ${`P(c|w)`(c, w, optimize = false)}") }

    //for (ln <- io.Source.stdin.getLines) println(predict(ln))

  }
}
