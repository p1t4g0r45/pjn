package pjn.lab5

import scala.collection.immutable.{Seq => ISeq}
import scala.io.{Codec, Source}
import scala.util.Try
import scala.util.matching.Regex

/**
  * Created by baran on 30.03.17.
  */
object MarkovChainsApp {

  import Levenshtein.distance

  val fileEnding = "" // ".test"

  val ngram_1 = "pjn_lab5/polish_model.lm.1"
  val ngram_2 = "pjn_lab5/polish_model.lm.2"
  val ngram_3 = "pjn_lab5/polish_model.lm.3"

  val improbable = -10d

  def `P(sentence)`(sentence: String) = {
    val words = sentence.split(" ").toList
    val beginning = if (words.isEmpty)
      0d
    else if (words.size == 1)
      `P(wj)bow`(words.head)
    else
      `P(wj)bow`(words.head) + `P(wj|wj-1)bow`(words(1), words.head)

    beginning + (
      if (words.size > 2)
        words.sliding(3).map {
          case List(wj2, wj1, wj) => `P(wj|wj-2, wj-1)bow`(wj, wj2, wj1)
        }.sum
      else 0d
      )
  }

  def `P(wj|wj-2, wj-1)bow`(wj: String, `wj-2`: String, `wj-1`: String): Double = `P(wj|wj-2, wj-1)`(wj, `wj-2`, `wj-1`) match {
    case Some(p) => p
    case _ => `BOW(wj-2, wj-1)`(`wj-2`, `wj-1`).getOrElse(improbable) + `P(wj|wj-1)bow`(wj, `wj-1`)
  }

  def `P(wj|wj-1)bow`(wj: String, `wj-1`: String): Double = `P(wj|wj-1)`(wj, `wj-1`) match {
    case Some(p) => p
    case _ => `BOW(wj-1)`(`wj-1`).getOrElse(improbable) + `P(wj)bow`(wj)
  }

  def `P(wj)bow`(wj: String): Double = `P(wj)`(wj).getOrElse(improbable)

  def `P(wj|wj-2, wj-1)`(wj: String, `wj-2`: String, `wj-1`: String): Option[Double] = {
    val pattern = raw"""\s*(-?\d+\.?\d*)\s*${`wj-2`}\s*${`wj-1`}\s*$wj\s*""".r
    getDouble(ngram_3, pattern)
  }

  def `P(wj|wj-1)`(wj: String, `wj-1`: String): Option[Double] = {
    val pattern = raw"""\s*(-?\d+\.?\d*)\s*${`wj-1`}\s*$wj\s*-?\d*\.?\d*""".r
    getDouble(ngram_2, pattern)
  }

  def `P(wj)`(wj: String): Option[Double] = {
    val pattern = raw"""\s*(-?\d+\.?\d*)\s*$wj\s*-?\d*\.?\d*""".r
    getDouble(ngram_1, pattern)
  }

  def `BOW(wj-2, wj-1)`(`wj-2`: String, `wj-1`: String): Option[Double] = {
    val pattern = raw"""\s*-?\d+\.?\d*\s*${`wj-2`}\s*${`wj-1`}\s*(-?\d*\.?\d*)""".r
    getDouble(ngram_2, pattern)
  }

  def `BOW(wj-1)`(`wj-1`: String): Option[Double] = {
    val pattern = raw"""\s*-?\d+\.?\d*\s*${`wj-1`}\s*(-?\d*\.?\d*)""".r
    getDouble(ngram_1, pattern)
  }

  def getDouble(filename: String, pattern: Regex): Option[Double] = {
    println(pattern)
    fileIterator(filename)
      .find(line => pattern.pattern.matcher(line).matches)
      .flatMap(line => {
        val pattern(p) = line
        Try(p.toDouble).toOption
      })
  }

  def fileIterator(filename: String) = Source.fromFile(filename + fileEnding)(Codec.ISO8859).getLines()

  def main(args: Array[String]): Unit = {
    printDistance("ala ma kota a kot ma ale", "ala ma kotaa a kot ma")
    println(`P(sentence)`("jeden dwa osiem cztery milion"))
  }

  def printDistance(s1: String, s2: String) = println(s"$s1 -> $s2 : ${distance(s1, s2)}")
}

object Levenshtein {
  def minimum(i1: Int, i2: Int, i3: Int) = math.min(math.min(i1, i2), i3)

  def distance(s1: String, s2: String): Int = distance(s1.split(" ").toList, s2.split(" ").toList)

  def distance(s1: ISeq[String], s2: ISeq[String]): Int = {
    val dist = Array.tabulate(s2.length + 1, s1.length + 1) { (j, i) => if (j == 0) i else if (i == 0) j else 0 }

    for (j <- 1 to s2.length; i <- 1 to s1.length)
      dist(j)(i) = if (s2(j - 1) == s1(i - 1)) dist(j - 1)(i - 1)
      else minimum(dist(j - 1)(i) + 1, dist(j)(i - 1) + 1, dist(j - 1)(i - 1) + 1)

    dist(s2.length)(s1.length)
  }
}