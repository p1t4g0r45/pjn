package pjn.lab10

import java.io.File
import javax.xml.parsers.DocumentBuilderFactory

import com.sun.org.apache.xerces.internal.dom.DeferredElementImpl
import org.w3c.dom.Node

import scala.io.{Codec, Source}

/**
  * Created by baran on 31.05.17.
  */
object PrepositionApp {
  val Filename = "pjn_lab10/out.xml"

  def fileIterator() = Source.fromFile(Filename)(Codec.ISO8859).getLines()

  def parse() = DocumentBuilderFactory.newInstance()
    .newDocumentBuilder()
    .parse(new File(Filename))

  def seg2word(segment: Node) = segment.asInstanceOf[DeferredElementImpl]
    .getElementsByTagName("f").item(0).asInstanceOf[DeferredElementImpl]
    .getElementsByTagName("string").item(0).getTextContent

  def seg2label(segment: Node) = segment.asInstanceOf[DeferredElementImpl]
    .getElementsByTagName("f").item(3).asInstanceOf[DeferredElementImpl]
    .getElementsByTagName("symbol").item(0).asInstanceOf[DeferredElementImpl]
    .getAttribute("value")

  def seg2category(segment: Node) = segment.asInstanceOf[DeferredElementImpl]
    .getElementsByTagName("f").item(2).asInstanceOf[DeferredElementImpl]
    .getElementsByTagName("symbol").item(0).asInstanceOf[DeferredElementImpl]
    .getAttribute("value")

  def getCase(label: String) = label.split(":").head

  def isPreposition(category: String) = category match {
    case "Prep" => true
    case _ => false
  }

  def getPrepositionCases(list: Seq[Node]) = list
    .filter(node => isPreposition(seg2category(node))) // filter prepositions
    .map(node => seg2word(node).toLowerCase -> getCase(seg2label(node))) // word -> case
    .groupBy { case (word, _) => word } // group by words
    .mapValues(values => values.map(_._2).groupBy(identity).mapValues(_.length)) // map values by cases
    .toList
    .sortBy { case (_, map) => map.values.sum } // sort by most often
    .reverse
    .map { case (word, map) => word -> map.toList.sortBy { case (_, amount) => amount }.reverse } // sort within each preposition by most used case

  def main(args: Array[String]): Unit = {
    val segments = parse().getElementsByTagName("seg")
    val list = for (i <- 0 until segments.getLength) yield segments.item(i)
    getPrepositionCases(list).foreach(println)
  }

  /**
    * nom - mianownik - kto? co?
    * gen - dopelniacz - kogo? czego?
    * dat - celownik - komu? czemu?
    * acc - biernik - kogo? co?
    * inst - narzednik - z kim? z czym?
    * loc - miejscownik - o kim? o czym?
    * voc - wolacz - o!
    */

  /**
    * (w,List((loc,21440), (acc,3176), (inst,4), (gen,2)))
    * (na,List((acc,7546), (loc,5893), (gen,11), (inst,1)))
    * (z,List((gen,7461), (inst,5777), (acc,6), (loc,2)))
    * (do,List((gen,10109)))
    * (o,List((loc,3918), (acc,2106), (gen,1)))
    * (po,List((loc,2626), (acc,445), (inst,1)))
    * (od,List((gen,2933)))
    * (za,List((acc,1898), (inst,768), (gen,68)))
    * (przez,List((acc,2694), (inst,2)))
    * (dla,List((gen,2103)))
    * (ze,List((gen,650), (inst,617), (acc,2)))
    * (przy,List((loc,1127)))
    * (przed,List((inst,1064), (acc,43)))
    * (pod,List((inst,689), (acc,223)))
    * (nad,List((inst,725), (acc,39)))
    * (bez,List((gen,709)))
    * (we,List((loc,443), (acc,116)))
    * (u,List((gen,548)))
    * (podczas,List((gen,376)))
    * (jak,List((nom,348)))
    * (między,List((inst,314), (acc,31)))
    * (według,List((gen,322)))
    * (wśród,List((gen,285)))
    * (dzięki,List((dat,256)))
    * (temu,List((acc,251)))
    * (wobec,List((gen,232)))
    * (poza,List((inst,180), (acc,28)))
    * (niż,List((nom,184)))
    */
}
