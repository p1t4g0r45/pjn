package util

/**
  * Created by baran on 24.10.17.
  */
object SortHelper {
  def bottom(n: Int, li: List[(String, Double)]): List[(String, Double)] = {

    def updateSofar(sofar: List[(String, Double)], el: (String, Double)): List[(String, Double)] = {
      // println (el + " - " + sofar)
      if (el._2 < sofar.head._2)
        (el :: sofar.tail).sortWith(_._2 > _._2)
      else sofar
    }

    (li.take(n).sortWith(_._2 > _._2) /: li.drop(n)) (updateSofar)
  }

  def top(n: Int, li: List[(String, Double)]): List[(String, Double)] = {

    def updateSofar(sofar: List[(String, Double)], el: (String, Double)): List[(String, Double)] = {
      // println (el + " - " + sofar)
      if (el._2 > sofar.head._2)
        (el :: sofar.tail).sortWith(_._2 < _._2)
      else sofar
    }

    (li.take(n).sortWith(_._2 < _._2) /: li.drop(n)) (updateSofar)
  }
}
