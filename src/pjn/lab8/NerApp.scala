package pjn.lab8

import morfologik.stemming.polish.PolishStemmer

import scala.io.{Codec, Source}

/**
  * Created by baran on 18.05.17.
  */
object NerApp {
  val NameEntitiesFilename = "pjn_lab8/named_entities"
  val Filename = "pjn_lab8/potop.txt"

  def fileIterator(filename: String) = Source.fromFile(filename)(Codec.ISO8859).getLines()

  val stemmer = new PolishStemmer

  def stem(word: String) = {
    val lookup = stemmer.lookup(word)
    if (lookup.size() == 0) {
      word
    } else {
      lookup.get(0).getStem.toString
    }
  }

  def getWords(fileName: String) = Source.fromFile(fileName).getLines.flatMap(_.split(" ")
    .map(word => word.toLowerCase.replaceAll("[^\\p{L}]+", "")))
    .filter(word => word.matches("[\\p{L}]+"))
    .toList

  def findOccurances(ne: String, words: List[String]) = {
    val onSide = 4
    val text = ne.toLowerCase.split(" ")
    words.sliding(onSide * 2 + text.length)
      .filter(ctx => ctx.takeRight(onSide + text.length).take(text.length) sameElements text)
      .map(_.mkString(" "))
      .toList
  }

  // http://ws.clarin-pl.eu/detextor.shtml
  // wyznaczanie nazw własnych - posortowana lista nazw

  // xml - http://ws.clarin-pl.eu/ner.shtml#
  def main(args: Array[String]): Unit = {
    val words = getWords(Filename)
    for (ln <- io.Source.stdin.getLines) findOccurances(ln, words).foreach(println)
  }
}
